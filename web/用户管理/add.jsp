<%@ page contentType="application/json;charset=UTF-8" language="java" %>
<%@ page import="javax.servlet.http.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<fmt:requestEncoding value="UTF8" />
<sql:setDataSource var="snapshot" driver="com.mysql.jdbc.Driver"
                   url="jdbc:mysql://dedsec.club:3306/xiaoqu?useUnicode=true&characterEncoding=utf-8"
                   user="root" password="xiaoqu@1"/>
<c:catch var="error">
<sql:update dataSource="${snapshot}" var="result">
insert into admin set useraccount = ?, userpassword = ?<sql:param value="${sqlParam.account}"/><sql:param value="${sqlParam.passwd}"/><c:if test="${sqlParam.userControl!=null}">, userControl = ?<sql:param value="${sqlParam.userControl}"/></c:if><c:if test="${sqlParam.name!=null}">,name = ?<sql:param value="${sqlParam.name}"/></c:if><c:if test="${sqlParam.ownerControl!=null}">, ownerControl = ?<sql:param value="${sqlParam.ownerControl}"/></c:if><c:if test="${sqlParam.complainControl!=null}">, complainControl = ?<sql:param value="${sqlParam.complainControl}"/></c:if><c:if test="${sqlParam.instrumentControl!=null}">, instrumentControl = ?<sql:param value="${sqlParam.instrumentControl}"/></c:if><c:if test="${sqlParam.carParkControl!=null}">, carParkControl = ?<sql:param value="${sqlParam.carParkControl}"/></c:if><c:if test="${sqlParam.chargeControl!=null}">, chargeControl = ?<sql:param value="${sqlParam.chargeControl}"/></c:if><c:if test="${sqlParam.buildingControl!=null}">, buildingControl = ?<sql:param value="${sqlParam.buildingControl}"/></c:if>;</sql:update>
</c:catch>

<c:choose>
<c:when test='${error!=null}'>
<c:set var="error" value="${error}"/>
<%
    String err = pageContext.getAttribute("error").toString();
    String[] part = err.split(":");
    StringBuilder res = new StringBuilder();
    for(int i = 2; i < part.length; i++) {
        res.append(part[i]).append("|");
    }
%>
<%="{\"status\":\""+res+"\"}"%>
</c:when>
<c:otherwise>{"status":"ok"}</c:otherwise>
</c:choose>