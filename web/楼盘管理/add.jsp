<%@ page contentType="application/json;charset=UTF-8" language="java" %>
<%@ page import="javax.servlet.http.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<fmt:requestEncoding value="UTF8" />
<sql:setDataSource var="snapshot" driver="com.mysql.jdbc.Driver"
                   url="jdbc:mysql://dedsec.club:3306/xiaoqu?useUnicode=true&characterEncoding=utf-8"
                   user="root" password="xiaoqu@1"/>


<c:catch var="error">
<sql:update dataSource="${snapshot}" var="result">
insert into buildingManagement set serverAddress = ?, buildingArea = ?, usedArea = ?, fixedCondition = ?, singlePrice = ?, totalPrice = ?, selledOut = ?<sql:param value="${sqlParam.serverAddress}"/><sql:param value="${sqlParam.buildingArea}"/><sql:param value="${sqlParam.usedArea}"/><sql:param value="${sqlParam. fixedCondition}"/><sql:param value="${sqlParam. singlePrice}"/><sql:param value="${sqlParam. totalPrice}"/><sql:param value="${sqlParam. selledOut}"/><c:if test="${sqlParam.remark!=null}">,remark = ?<sql:param value="${sqlParam.remark}"/></c:if><c:if test="${sqlParam.roomNum !=null}">,roomNum = ?<sql:param value="${sqlParam.roomNum}"/></c:if><c:if test="${sqlParam.buyerNum!=null}">,buyerNum = ?<sql:param value="${sqlParam.buyerNum}"/></c:if><c:if test="${sqlParam.buyerName!=null}">,buyerName = ?<sql:param value="${sqlParam.buyerName}"/></c:if>;</sql:update>
</c:catch>

<c:choose>
<c:when test='${error!=null}'>
<c:set var="error" value="${error}"/>
<%
    String err = pageContext.getAttribute("error").toString();
    String[] part = err.split(":");
    StringBuilder res = new StringBuilder();
    for(int i = 2; i < part.length; i++) {
        res.append(part[i]).append("|");
    }
%>
<%="{\"status\":\""+res+"\"}"%>
</c:when>
<c:otherwise>{"status":"ok"}</c:otherwise>
</c:choose>