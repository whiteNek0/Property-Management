<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="application/json;charset=UTF-8" language="java" %>
<sql:setDataSource var="snapshot" driver="com.mysql.jdbc.Driver"
                   url="jdbc:mysql://dedsec.club:3306/xiaoqu?useUnicode=true&characterEncoding=utf-8"
                   user="root" password="xiaoqu@1"/>
<sql:query dataSource="${snapshot}" var="result">
select name,userControl, ownerControl,chargeControl, instrumentControl, carParkControl, buildingControl, complainControl,userSee, ownerSee,chargeSee, instrumentSee, carParkSee, buildingSee, complainSee from admin where useraccount = ? and userpassword = ?;
<sql:param value="${param.account}"/>
<sql:param value="${param.passwd}"/>
</sql:query>
[
<c:forEach var="row" items="${result.rows}" varStatus="stat">
{
"name": "<c:out value="${row.name}"/>",
"auth":{
"用户管理": "<c:out value="${row.userControl}"/>",
"住户管理": "<c:out value="${row.ownerControl}"/>",
"物业收费管理": "<c:out value="${row.chargeControl}"/>",
"仪表数据管理": "<c:out value="${row.instrumentControl}"/>",
"停车场管理": "<c:out value="${row.carParkControl}"/>",
"楼盘管理": "<c:out value="${row.buildingControl}"/>",
"业主投诉管理": "<c:out value="${row.complainControl}"/>"
},
"see":{
"用户管理": "<c:out value="${row.userSee}"/>",
"住户管理": "<c:out value="${row.ownerSee}"/>",
"物业收费管理": "<c:out value="${row.chargeSee}"/>",
"仪表数据管理": "<c:out value="${row.instrumentSee}"/>",
"停车场管理": "<c:out value="${row.carParkSee}"/>",
"楼盘管理": "<c:out value="${row.buildingSee}"/>",
"业主投诉管理": "<c:out value="${row.complainSee}"/>"
}
}<c:if test="${!stat.last}">,</c:if>
</c:forEach>
]