<%@ page contentType="application/json;charset=UTF-8" language="java" %>
<%@ page import="javax.servlet.http.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<sql:setDataSource var="snapshot" driver="com.mysql.jdbc.Driver"
                   url="jdbc:mysql://dedsec.club:3306/xiaoqu?useUnicode=true&characterEncoding=utf-8"
                   user="root" password="xiaoqu@1"/>

<c:catch var="error">
<sql:update dataSource="${snapshot}" var="result">
delete from complain where complainNum = ?;
<sql:param value="${param.id}"/>
</sql:update>
</c:catch>

<c:choose>
<c:when test='${error!=null}'>
<c:set var="error" value="${error}"/>
<%
    String err = pageContext.getAttribute("error").toString();
    String[] part = err.split(":");
    StringBuilder res = new StringBuilder();
    for(int i = 2; i < part.length; i++) {
        res.append(part[i]).append("|");
    }
%>
<%="{\"status\":\""+res+"\"}"%>
</c:when>
<c:otherwise>{"status":"ok"}</c:otherwise>
</c:choose>