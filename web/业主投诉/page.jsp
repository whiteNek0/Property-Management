<%@ page contentType="application/json;charset=UTF-8" language="java" %>
<%@ page import="javax.servlet.http.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<sql:setDataSource var="snapshot" driver="com.mysql.jdbc.Driver"
                   url="jdbc:mysql://dedsec.club:3306/xiaoqu?useUnicode=true&characterEncoding=utf-8"
                   user="root" password="xiaoqu@1"/>

<fmt:parseNumber value="${param.page_no}" var="pageNumber"/>
<sql:query dataSource="${snapshot}" var="result">


SELECT complainNum,complainDate, ownerName,serverName, serverAddress, complainTel,complainReason,solvePeople,solveDate,result from complain limit ?, 5;
<sql:param value="${pageNumber}"/>
</sql:query>
[
<c:forEach var="row" items="${result.rows}" varStatus="stat">
{
"投诉编号": "<c:out value="${row.complainNum}"/>",
"投诉日期": "<c:out value="${row.complainDate}"/>",
"投诉用户": "<c:out value="${row.ownerName}"/>",
"接待人员": "<c:out value="${row.serverName}"/>",
"物业地址": "<c:out value="${row.serverAddress}"/>",
"电话": "<c:out value="${row.complainTel}"/>",
"投诉内容": "<c:out value="${row.complainReason}"/>",
"处理人员": "<c:out value="${row.solvePeople}"/>",
"处理日期":"<c:out value="${row.solveDate}"/>",
"处理情况": "<c:out value="${row.result}"/>"
}<c:if test="${!stat.last}">,</c:if>
</c:forEach>
]