<%@ page contentType="application/json;charset=UTF-8" language="java" %>
<%@ page import="javax.servlet.http.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<fmt:requestEncoding value="UTF8" />
<sql:setDataSource var="snapshot" driver="com.mysql.jdbc.Driver"
                   url="jdbc:mysql://dedsec.club:3306/xiaoqu?useUnicode=true&characterEncoding=utf-8"
                   user="root" password="xiaoqu@1"/>
<c:catch var="error">
<sql:update dataSource="${snapshot}" var="result">
insert into instrumentData_water set ownerName = ?, serverAddress = ?, handleDate = ?, dosage = ?, price = ?<sql:param value="${sqlParam.ownerName}"/><sql:param value="${sqlParam.serverAddress}"/><sql:param value="${sqlParam.handleDate}"/><sql:param value="${sqlParam.dosage}"/><sql:param value="${sqlParam.price}"/><c:if test="${sqlParam.waterid!=null}">,id = ?<sql:param value="${sqlParam.waterid}"/></c:if><c:if test="${sqlParam.lastMonthRecord!=null}">, lastMonthRecord = ?<sql:param value="${sqlParam.lastMonthRecord}"/></c:if><c:if test="${sqlParam.handleGuy!=null}">, handleGuy = ?<sql:param value="${sqlParam.handleGuy}"/></c:if><c:if test="${sqlParam.monthPayed!=null}">, monthPayed = ?<sql:param value="${sqlParam.monthPayed}"/></c:if>;</sql:update>
</c:catch>

<c:choose>
<c:when test='${error!=null}'>
<c:set var="error" value="${error}"/>
<%
    String err = pageContext.getAttribute("error").toString();
    String[] part = err.split(":");
    StringBuilder res = new StringBuilder();
    for(int i = 2; i < part.length; i++) {
        res.append(part[i]).append("|");
    }
%>
<%="{\"status\":\""+res+"\"}"%>
</c:when>
<c:otherwise>{"status":"ok"}</c:otherwise>
</c:choose>