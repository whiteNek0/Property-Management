<%@ page contentType="application/json;charset=UTF-8" language="java" %>
<%@ page import="javax.servlet.http.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<sql:setDataSource var="snapshot" driver="com.mysql.jdbc.Driver"
                   url="jdbc:mysql://dedsec.club:3306/xiaoqu?useUnicode=true&characterEncoding=utf-8"
                   user="root" password="xiaoqu@1"/>

<fmt:parseNumber value="${param.page_no}" var="pageNumber"/>
<sql:query dataSource="${snapshot}" var="result">
SELECT id,ownerName, serverAddress,monthPayed, hyear, hmonth, dosage, price, fee,lastMonthRecord, handleGuy from instrumentData_water limit ?, 5;
<sql:param value="${pageNumber}"/>
</sql:query>
[
<c:forEach var="row" items="${result.rows}" varStatus="stat">
    {
        "仪表编号": "<c:out value="${row.id}"/>",
        "住户姓名": "<c:out value="${row.ownerName}"/>",
        "物业地址": "<c:out value="${row.serverAddress}"/>",
        "年份": "<c:out value="${row.hyear}"/>",
        "月份": "<c:out value="${row.hmonth}"/>",
        <%--"上月数据": "<c:out value="${row.locate}"/>",--%>
        <%--"本月数据": "<c:out value="${row.term}"/>",--%>
        "本月用量": "<c:out value="${row.dosage}"/>",
        "单价": "<c:out value="${row.price}"/>",
        "本月费用": "<c:out value="${row.fee}"/>",
        "上月抄表日期":"<c:out value="${row.lastMonthRecord}"/>",
        "本月交费日期": "<c:out value="${row.monthPayed}"/>",
        "办理人": "<c:out value="${row.handleGuy}"/>"
    }<c:if test="${!stat.last}">,</c:if>
</c:forEach>
]