<%@ page contentType="application/json;charset=UTF-8" language="java" %>
<%@ page import="javax.servlet.http.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<sql:setDataSource var="snapshot" driver="com.mysql.jdbc.Driver"
                   url="jdbc:mysql://dedsec.club:3306/xiaoqu?useUnicode=true&characterEncoding=utf-8"
                   user="root" password="xiaoqu@1"/>

<fmt:parseNumber value="${param.page_no}" var="pageNumber"/>
<sql:query dataSource="${snapshot}" var="result">
SELECT ownerManage_Owner.ownerName,ownerID, birthday, nativePlace, sex, workPlace,address, postcode,idNumber, tel, bank,bankAccount,checkinTime,outTime from ownerManage_Owner
JOIN ownerManage_Bank ON ownerManage_Owner.ownerName = ownerManage_Bank.ownerName
JOIN ownerManage_Time ON ownerManage_Owner.ownerName = ownerManage_Time.ownerName     limit ?, 5;
<sql:param value="${pageNumber}"/>
</sql:query>
[
<c:forEach var="row" items="${result.rows}" varStatus="stat">
{
"业主姓名": "<c:out value="${row.ownerName}"/>",
"业主ID": "<c:out value="${row.ownerID}"/>",
"出生年月": "<c:out value="${row.birthday}"/>",
"籍贯": "<c:out value="${row.nativePlace}"/>",
"性别": "<c:out value="${row.sex}"/>",
"工作单位": "<c:out value="${row.workPlace}"/>",
"地址": "<c:out value="${row.address}"/>",
"邮编": "<c:out value="${row.postcode}"/>",
"身份证":"<c:out value="${row.idNumber}"/>",
"联系电话": "<c:out value="${row.tel}"/>",
"开户银行": "<c:out value="${row.bank}"/>",
"银行账号": "<c:out value="${row.bankAccount}"/>",
"入住时间":"<c:out value="${row.checkinTime}"/>",
"迁出时间": "<c:out value="${row.outTime}"/>"
}<c:if test="${!stat.last}">,</c:if>
</c:forEach>
]